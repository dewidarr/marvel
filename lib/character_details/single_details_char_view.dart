import 'package:flutter/material.dart';
import 'package:marvel/models/character_data.dart';
import 'package:marvel/models/details_data.dart';

class DetailsItemView extends StatelessWidget {
  DetailsItemView({@required this.detailsData,@required this.thumbnail});
  final DetailsData detailsData;
  final Thumbnail thumbnail;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Container(
        width: 100.0,
        child: Column(
          children: <Widget>[
            Image.network(thumbnail.path+'.'+thumbnail.extension,height: 160.0,fit: BoxFit.fill,) ,
            Expanded(child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: SingleChildScrollView(child: Text(detailsData.title,style: Theme.of(context).textTheme.caption.copyWith(color: Colors.white),)),
            ))
          ],
        ),
      ),
    );
  }
}
