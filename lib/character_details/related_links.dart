import 'package:flutter/material.dart';
import 'package:marvel/character_details/details_contract.dart';
import 'package:marvel/character_details/details_presenter.dart';
import 'package:marvel/character_details/single_details_char_view.dart';
import 'package:marvel/localization/applocaliz.dart';
import 'package:marvel/models/details_data.dart';
import 'package:url_launcher/url_launcher.dart';


class RelatedLinksView extends StatefulWidget {
RelatedLinksView({@required this.charId});
 final String charId;

  @override
  _RelatedLinksViewState createState() => _RelatedLinksViewState();
}

class _RelatedLinksViewState extends State<RelatedLinksView> implements IDetailsContract{
  List<RelatedLinksData> _relatedLinksList=[];
  DetailsPresenter _detailsPresenter;
@override
  void initState() {
    super.initState();
    _detailsPresenter = DetailsPresenter.relatedLinks(this,widget.charId);
    _detailsPresenter.getRelatedLinks();
  }

  @override
  Widget build(BuildContext context) {
    TextStyle captionStyle =
    Theme.of(context).textTheme.caption.copyWith(color: Color(0xffe2151f));
    TextStyle infoStyle =
    Theme.of(context).textTheme.title.copyWith(color: Colors.white);

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(AppLocalizations.of(context).translate('related').toUpperCase(),style: captionStyle.copyWith(fontWeight: FontWeight.w500),),
          relatedView(context, infoStyle,'detail'),
          relatedView(context, infoStyle,'wiki'),
          relatedView(context, infoStyle,'comiclink'),
        ],
      ),
    );
  }

  Widget relatedView(BuildContext context, TextStyle infoStyle,String linkName) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical:20.0),
      child: GestureDetector(
        onTap: (){
          generatLink(linkName);
        },
        child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
              Text(AppLocalizations.of(context).translate(linkName),style: infoStyle,),
                Icon(Icons.arrow_forward_ios,color: Colors.white,)
            ],),
      ),
    );
  }

  @override
  void addRelatedLinks(List<RelatedLinksData> relatedLinksList) {
    setState(() {
      _relatedLinksList = relatedLinksList;
    });
  }


  @override
  void addComicsRecord(List<DetailsItemView> comicsList) {
    // TODO: implement addComicsRecord
  }

  @override
  void hideCategorySection() {
    // TODO: implement hideCategorySection
  }

  @override
  void hideLoading() {
    // TODO: implement hideLoading
  }

  @override
  void showCategorySection() {
    // TODO: implement showCategorySection
  }

  @override
  void showLoading() {
    // TODO: implement showLoading
  }

  void generatLink(String linkName) async{
    print('tapped');
    _relatedLinksList.forEach((link)async{
      if(linkName ==link.type){
        print(link.url);
        if (await canLaunch(link.url)) {
      await launch(link.url);
      } else {
      throw 'Could not launch $url';
      }
      }
    });

  }

}

