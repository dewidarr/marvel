import 'package:flutter/material.dart';
import 'package:marvel/character_details/details_contract.dart';
import 'package:marvel/character_details/details_presenter.dart';
import 'package:marvel/character_details/single_details_char_view.dart';
import 'package:marvel/localization/applocaliz.dart';
import 'package:marvel/models/details_data.dart';

class CategoriesViewHandler extends StatefulWidget {
  CategoriesViewHandler({@required this.charId,@required this.categoryName});
 final String charId;
 final String categoryName;
  @override
  _CategoriesViewHandlerState createState() => _CategoriesViewHandlerState();
}

class _CategoriesViewHandlerState extends State<CategoriesViewHandler> implements IDetailsContract{
  bool _isLoading =false;
  bool _isCategoryEmpty = false;
  List<DetailsItemView> _comicsList = [];
  DetailsPresenter _detailsPresenter;
  ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    _detailsPresenter = DetailsPresenter(this,characterId: widget.charId,categoryName:widget.categoryName);
    _detailsPresenter.setUp();
  }

  @override
  Widget build(BuildContext context) {
    TextStyle captionStyle =
    Theme.of(context).textTheme.caption.copyWith(color: Color(0xffe2151f),fontWeight: FontWeight.w600);
    TextStyle infoStyle =
    Theme.of(context).textTheme.body2.copyWith(color: Colors.white);
    return  _isCategoryEmpty? Container():
    Padding(
      padding: const EdgeInsets.only(left:8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom:8.0),
            child: Text(
              AppLocalizations.of(context).translate(widget.categoryName).toUpperCase(),
              style: captionStyle,
            ),
          ),
          Container(
            height: 260.0,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: ListView.builder(
                    controller: _controller,
                    scrollDirection: Axis.horizontal,
                    itemCount: _comicsList.length,
                    itemBuilder: (_,index){
                      return _comicsList[index];
                    },
                  ),
                ),
                _isLoading? Center(
                  child: CircularProgressIndicator(valueColor:AlwaysStoppedAnimation<Color>(Colors.white)),
                ):Container(),
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  void addComicsRecord(List<DetailsItemView> comicsList) {
    setState(() {
      _comicsList.addAll(comicsList);
    });
  }


  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      _detailsPresenter.setUp();
    }

  }

  @override
  void hideLoading() {
   setState(() {
     _isLoading = false;
   });
  }

  @override
  void showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  @override
  void hideCategorySection() {
  setState(() {
    _isCategoryEmpty = true;
  });
  }

  @override
  void showCategorySection() {
   setState(() {
     _isCategoryEmpty = false;
   });
  }

  @override
  void addRelatedLinks(List<RelatedLinksData> relatedLinksList) {
    // TODO: implement addRelatedLinks
  }
}
