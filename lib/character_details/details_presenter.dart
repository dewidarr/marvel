import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:marvel/character_details/details_contract.dart';
import 'package:marvel/character_details/single_details_char_view.dart';
import 'package:marvel/mixins/fetch_data_mixin.dart';
import 'package:marvel/models/character_data.dart';
import 'package:marvel/models/details_data.dart';

import '../consts.dart';

const url = 'https://gateway.marvel.com:443/v1/public/characters/';

class DetailsPresenter with FetchDataMixin{
  final _itemsLimit = 15;
  int _page = 0;
  int _offset = 0;

  DetailsPresenter(this._detailsContract,
      {this.characterId, this.categoryName});

  DetailsPresenter.relatedLinks(this._detailsContract,this.characterId);

  IDetailsContract _detailsContract;
  String characterId;
  String categoryName;

  void setUp() async {
    _offset = _page * _itemsLimit;
    _detailsContract.showLoading();
    http.Response response = await http.get(getQuery());
    final List<DetailsItemView> detailsList = [];
    if (response.statusCode == 200) {
      var decodeData = jsonDecode(response.body);
      List<dynamic> data = decodeData['data']['results'];
      if (data == null) {
        _detailsContract.hideLoading();
      }

      for (var detailsRecord = 0;
          detailsRecord < data.length;
          detailsRecord++) {
        DetailsData detailsData = DetailsData.fromJson(data[detailsRecord]);
        Thumbnail thumbnail =
            Thumbnail.fromJson(data[detailsRecord]['thumbnail']);
        /*
        * check if image thumbnail comics,stories,series or events not empty
        * if it empty then remove the specific section
        * */
        if (thumbnail.path != null) {
          _detailsContract.showCategorySection();
          detailsList.add(DetailsItemView(
            detailsData: detailsData,
            thumbnail: thumbnail,
          ));
        } else {
          _detailsContract.hideCategorySection();
        }
      }
      _detailsContract.addComicsRecord(detailsList);
      _detailsContract.hideLoading();
      _page++;
    }
  }

  void getRelatedLinks() async {
    http.Response response = await http.get(getRelatedLinksQuery());
    final List<RelatedLinksData> relatedLinksList = [];
    if (response.statusCode == 200) {
      var decodeData = jsonDecode(response.body);
      List<dynamic> data = decodeData['data']['results'][0]['urls'];

      for (int linksRecord = 0; linksRecord < data.length; linksRecord++) {
        RelatedLinksData relatedLinksData =
            RelatedLinksData.fromJson(data[linksRecord]);
        relatedLinksList.add(relatedLinksData);
      }
      _detailsContract.addRelatedLinks(relatedLinksList);
    }
  }

  getQuery() {
    return url +
        characterId +
        '/$categoryName?limit=$_itemsLimit&offset=$_offset&ts=$Kts&apikey=$KPublic_Key&hash=' +
        generateMd5(KMd5Data);
  }

  getRelatedLinksQuery() {
    return url +
        characterId +
        '?ts=$Kts&apikey=$KPublic_Key&hash='+
        generateMd5(KMd5Data);
  }


}
