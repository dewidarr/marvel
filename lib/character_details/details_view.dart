import 'package:flutter/material.dart';
import 'package:marvel/character_details/categories_views_handler.dart';
import 'package:marvel/character_details/related_links.dart';
import 'package:marvel/localization/applocaliz.dart';
import 'package:marvel/models/character_data.dart';
import 'package:marvel/models/details_data.dart';

class CharacterDetailsScreen extends StatefulWidget {
  static String id = 'char_details';

  CharacterDetailsScreen({this.characterData, this.thumbnail});

  final CharacterData characterData;
  final Thumbnail thumbnail;

  @override
  _CharacterDetailsScreenState createState() => _CharacterDetailsScreenState();
}

class _CharacterDetailsScreenState extends State<CharacterDetailsScreen> {
  final double _imageHeight = 300.0;
  String _collapsedImageUrl;

  @override
  void initState() {
    super.initState();
    _collapsedImageUrl =
        widget.thumbnail.path + '.' + widget.thumbnail.extension;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff282c30),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: Colors.transparent.withOpacity(0.5),
            flexibleSpace: collapsedImage(),
            expandedHeight: _imageHeight,
            pinned: true,
            centerTitle: true,
            title: Text(widget.characterData.name,style: Theme.of(context).textTheme.title.copyWith(color: Colors.white),),
          ),
          SliverList(delegate: SliverChildListDelegate(showDetailsViews()))
        ],
      ),
    );
  }

  Widget collapsedImage() {
    return FlexibleSpaceBar(
      background: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.fill, image: NetworkImage(_collapsedImageUrl))),
      ),
    );
  }

  List<Widget> showDetailsViews() {
    TextStyle captionStyle =
        Theme.of(context).textTheme.caption.copyWith(color: Color(0xffe2151f),fontWeight: FontWeight.w600);
    TextStyle infoStyle =
        Theme.of(context).textTheme.body2.copyWith(color: Colors.white);
    /*
    * return list of widget
    * CategoriesViewHandler  is stateful widget to fetch lists for comics,series, stories and events
    * */
    final List<Widget> allDetails = [];

    allDetails.add(Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            AppLocalizations.of(context).translate('name').toUpperCase(),
            style: captionStyle,
          ),
          SizedBox(
            height: 10.0,
          ),
          Text(
            widget.characterData.name,
            style: infoStyle,
          ),
          SizedBox(
            height: 10.0,
          ),
          Text(
            AppLocalizations.of(context).translate('desc').toUpperCase(),
            style: captionStyle,
          ),
          SizedBox(
            height: 10.0,
          ),
          Text(
            widget.characterData.description,
            style: infoStyle,
          )
        ],
      ),
    ));
    allDetails.add( CategoriesViewHandler(charId: widget.characterData.id.toString(),categoryName: 'comics',));
    allDetails.add( CategoriesViewHandler(charId: widget.characterData.id.toString(),categoryName: 'series',));
    allDetails.add( CategoriesViewHandler(charId: widget.characterData.id.toString(),categoryName: 'stories',));
    allDetails.add( CategoriesViewHandler(charId: widget.characterData.id.toString(),categoryName: 'events',));
    // related link view
    allDetails.add(RelatedLinksView(charId: widget.characterData.id.toString(),));

    return allDetails;
  }


}
