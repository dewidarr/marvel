import 'package:marvel/character_details/single_details_char_view.dart';
import 'package:marvel/models/details_data.dart';

abstract class IDetailsContract{
  void addComicsRecord(List<DetailsItemView> comicsList);
  void showLoading();
  void hideLoading();
  void hideCategorySection();
  void showCategorySection();
  void addRelatedLinks(List<RelatedLinksData> relatedLinksList);
}