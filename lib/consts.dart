import 'package:flutter/material.dart';

const Kts = 'dewa';
const KPublic_Key = 'da7659729bb47ae56a4d093e6cc9b213';
const KPrivate_Key = '52550cdf574dfbaf7130168bde7f813904c9b82a';
const KMd5Data = Kts + KPrivate_Key + KPublic_Key;

const KSearchFieldDecoration = InputDecoration(
  contentPadding: EdgeInsets.symmetric( horizontal: 8.0),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(10.0)),
  ),
  enabledBorder: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(10.0)),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.black45, width: 1.0),
    borderRadius: BorderRadius.all(Radius.circular(10.0)),
  ),
);