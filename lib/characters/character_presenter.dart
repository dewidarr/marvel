import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:marvel/mixins/fetch_data_mixin.dart';
import 'package:marvel/models/character_data.dart';

import '../consts.dart';
import 'characters_contract.dart';
import 'singleChar_view.dart';

const url = 'https://gateway.marvel.com:443/v1/public/characters';

class CharacterPresenter  with FetchDataMixin{
  String _hashKey ;
  final _itemsLimit = 15;
  int _page = 0;
  int _offset = 0;

  final ICharacterContract _iCharacterContract;

  CharacterPresenter(this._iCharacterContract);

  void setUp() async {
    _offset = _page*_itemsLimit;
    _iCharacterContract.showLoading();
      http.Response response = await http.get(getQuery());
      final List<CharacterItemView> characterList = [];
      if (response.statusCode == 200) {
        var decodeData = jsonDecode(response.body);
        List<dynamic> data = decodeData['data']['results'];
        if (data == null) {
          _iCharacterContract.hideLoading();
        }

        for (var chaRecord = 0; chaRecord < data.length; chaRecord++) {
          CharacterData characterData = CharacterData.fromJson(data[chaRecord]);
          Thumbnail thumbnail = Thumbnail.fromJson(data[chaRecord]['thumbnail']);

          characterList.add(CharacterItemView(
            characterData: characterData,
            thumbnail: thumbnail,
          ));
        }
        _iCharacterContract.addRecord(characterList);
        _iCharacterContract.hideLoading();
        _page++;
      }

  }
  void fetchSearchData(String charName)async{

    http.Response response = await http.get(getSearchQuery(charName));
    final List<SearchItemView> characterSearchList = [];
    if (response.statusCode == 200) {
      var decodeData = jsonDecode(response.body);
      List<dynamic> data = decodeData['data']['results'];

      for (var chaRecord = 0; chaRecord < data.length; chaRecord++) {
        CharacterData characterData = CharacterData.fromJson(data[chaRecord]);
        Thumbnail thumbnail = Thumbnail.fromJson(data[chaRecord]['thumbnail']);

        characterSearchList.add(SearchItemView(
          characterData: characterData,
          thumbnail: thumbnail,
        ));
      }
    _iCharacterContract.addSearchRecord(characterSearchList);
  }

}

  getSearchQuery(String name) {
    return 'https://gateway.marvel.com:443/v1/public/characters?nameStartsWith=$name&limit=$_itemsLimit&offset=$_offset&ts=$Kts&apikey=$KPublic_Key&hash=${ generateMd5(KMd5Data)}';

  }

  String getQuery() {
    return 'https://gateway.marvel.com:443/v1/public/characters?limit=$_itemsLimit&offset=$_offset&ts=$Kts&apikey=$KPublic_Key&hash=${ generateMd5(KMd5Data)}';
  }

}

