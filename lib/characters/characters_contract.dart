
import 'singleChar_view.dart';

abstract class ICharacterContract{
  void addRecord(List<CharacterItemView>characterView);
  void addSearchRecord(List<SearchItemView>characterSearchView);
  void showLoading();
  void hideLoading();
}