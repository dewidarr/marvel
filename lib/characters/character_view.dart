import 'package:flutter/material.dart';
import 'package:marvel/characters/singleChar_view.dart';
import 'package:marvel/characters/character_presenter.dart';
import 'package:marvel/characters/characters_contract.dart';
import 'package:marvel/consts.dart';
import 'package:marvel/localization/applocaliz.dart';



class CharactersScreen extends StatefulWidget {
  static String id = 'character_screen';


  @override
  _CharactersScreenState createState() => _CharactersScreenState();
}

class _CharactersScreenState extends State<CharactersScreen> implements ICharacterContract{
  bool _isLoading = false;
  List<CharacterItemView> _characterList = [];
  List<SearchItemView> _searchResultList = [];
  CharacterPresenter _characterPresenter;

 final TextEditingController _searchController = TextEditingController();
 bool _searching = false;
 String characterName ='';

  ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    _characterPresenter = CharacterPresenter(this);
    _characterPresenter.setUp();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff282c30),
      appBar:  AppBar(
        backgroundColor: Colors.black,
        automaticallyImplyLeading: false,
        title: _searching ? searchView() : marvelLogo(),
        centerTitle: true,
        iconTheme: IconThemeData(color: Color(0xfff12731)),
        actions: setActions(),
      ),
      body:_searching?
        ListView.builder(
          itemCount: _searchResultList.length,
          itemBuilder: (_,index){
            return _searchResultList[index];
          },
        ) :
      Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              controller: _controller,
              itemCount: _characterList.length,
                itemBuilder: (_,index){
             return _characterList[index];
            }),
          ),
          _isLoading? Container(
            color: Color(0xff282c30),
            width: double.infinity,
            height: 100.0,
            child: Center(child: CircularProgressIndicator(valueColor:AlwaysStoppedAnimation<Color>(Colors.white)),),
          ): Container()

        ],
      ),
    );
  }

  @override
  void addRecord(List<CharacterItemView> characterView) {
    setState(() {
   this._characterList.addAll(characterView);
    });
  }

  @override
  void hideLoading() {
    setState(() {
      this._isLoading = false;
    });
  }

  @override
  void showLoading() {
   setState(() {
     this._isLoading = true;
   });
  }

  @override
  void addSearchRecord(List<SearchItemView> characterSearchView) {

    setState(() {
      _searchResultList.clear();
      _searchResultList.addAll(characterSearchView);
    });
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
     _characterPresenter.setUp();
    }

  }

  Widget searchView() {
    return Container(
      height: 30.0,
      child: TextField(
        controller: _searchController,
        autofocus: true,
        decoration: KSearchFieldDecoration.copyWith(
          prefixIcon: Icon(Icons.search,color: Colors.black12,),
          filled: true,
          fillColor: Colors.white,
          hintText: AppLocalizations.of(context).translate('search'),
          hintStyle: TextStyle(color: Colors.black54),
        ),
        style: TextStyle(color: Colors.black54, fontSize: 16.0),
        onChanged: (data) => updateList(data),
      ),
    );
  }

  List<Widget> setActions() {
    if (_searching) {
      return <Widget>[
        GestureDetector(
          onTap: (){
            if (_searchController == null ||
                _searchController.text.isEmpty) {
              Navigator.pop(context);
              return;
            }
            clearSearchData();
          },
            child: Center(child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(AppLocalizations.of(context).translate('cancel'),style: TextStyle(color: Color(0xfff12731))),
            )))
      ];
    }

    return <Widget>[
      IconButton(
        icon: const Icon(Icons.search),
        onPressed: startSearching,
      ),
    ];
  }

  void startSearching() {
    ModalRoute.of(context)
        .addLocalHistoryEntry(LocalHistoryEntry(onRemove: stopSearching));
    setState(() {
      _searching = true;
    });
  }

  void updateList(String data) {
    _characterPresenter.fetchSearchData(data);
  }

  void stopSearching() {
    clearSearchData();

    setState(() {
      _searching = false;
    });
  }

  void clearSearchData() {
    setState(() {
      _searchController.clear();
      updateList("");
    });
  }

  Widget marvelLogo() {
    return Container(
      width: 100.0,
      height: 28.0,
      color: Color(0xfff12731),
      child: Center(child: Text('MARVEL',style: Theme.of(context).textTheme.headline.copyWith(color: Colors.white,letterSpacing:.1 ,fontWeight: FontWeight.w900 ),)),
    );
  }


}

