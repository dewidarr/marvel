import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:marvel/character_details/details_view.dart';
import 'package:marvel/models/character_data.dart';

class CharacterItemView extends StatelessWidget {
  CharacterItemView({@required this.characterData,@required this.thumbnail});
  final CharacterData characterData;
  final Thumbnail thumbnail;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap:(){
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CharacterDetailsScreen(characterData: characterData,thumbnail: thumbnail,))
        );
      },
      child: CachedNetworkImage(
        imageUrl: thumbnail.path +'.'+ thumbnail.extension,
        imageBuilder: (context, imageProvider) => Container(
          width: double.infinity,
          height: 150.0,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,
            ),
          ),
          child: Stack(
            children: <Widget>[
              Positioned(
                bottom: 16.0,
                left: 12.0,
                child: characterName(context),
              )
            ],
          ),
        ),
        errorWidget: (context, url, error) => Icon(Icons.error),
      ),
    );
  }

  Widget characterName(BuildContext context) => Material(
    color: Colors.white,
    child: Container(
      width: 120.0,
      height: 40.0,
      child: Column(
        children: <Widget>[
          Expanded(child: SingleChildScrollView(child: Center(child: Text(characterData.name,style: Theme.of(context).textTheme.title,)))),
        ],
      ),

    ),
  );
}

class SearchItemView extends StatelessWidget {
  SearchItemView({@required this.characterData,@required this.thumbnail});
  final CharacterData characterData;
  final Thumbnail thumbnail;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CharacterDetailsScreen(characterData: characterData,thumbnail: thumbnail,))
        );
      },
      child: Container(
        height: 50.0,
        color: Color(0xff3c3f43),
        child: Row(
          children: <Widget>[
            Image.network(thumbnail.path+'.'+ thumbnail.extension,width: 50.0,height: 50.0,),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(characterData.name,style: TextStyle(color: Colors.white),),
            )
          ],
        ),
      ),
    );
  }
}

