
import 'package:flutter/cupertino.dart';

class DetailsData{
  DetailsData({@required this.id,@required this.title,@required this.description});
  int id;
  String title;
  String description;

  DetailsData.fromJson(Map<String,dynamic> json){
    id= json['id'];
    title = json['title'];
    description=json['description'];
  }
}

class RelatedLinksData{

  String type;
  String url;

  RelatedLinksData.fromJson(Map<String,dynamic> json){
    type = json['type'];
    url = json['url'];
  }

}